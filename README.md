Spatial interpolation / prediction using Ensemble Machine Learning
================
Created and maintained by: Tom Hengl
(<a href="mailto:tom.hengl@OpenGeoHub.org" class="email">tom.hengl@OpenGeoHub.org</a>)
\|
Last compiled on: 09 March, 2021



-   [![alt text](tex/R_logo.svg.png "About")
    Introduction](#alt-text-introduction)
    -   [Ensemble Machine Learning](#ensemble-machine-learning)
    -   [Using geographical distances to improve spatial
        interpolation](#using-geographical-distances-to-improve-spatial-interpolation)
    -   [EML using the landmap package](#eml-using-the-landmap-package)
-   [![alt text](tex/R_logo.svg.png "Zinc") Example: Spatial prediction
    of Zinc meuse data
    set](#alt-text-example-spatial-prediction-of-zinc-meuse-data-set)
    -   [Interpolation of numeric values using spatial
        regression](#interpolation-of-numeric-values-using-spatial-regression)
    -   [Model fine-tuning and feature
        selection](#model-fine-tuning-and-feature-selection)
    -   [Estimation of prediction
        intervals](#estimation-of-prediction-intervals)
    -   [Predictions using log-transformed target
        variable](#predictions-using-log-transformed-target-variable)
-   [![alt text](tex/R_logo.svg.png "Zinc") Example: Spatial prediction
    of soil types
    (factor-variable)](#alt-text-example-spatial-prediction-of-soil-types-factor-variable)
    -   [EML for classification](#eml-for-classification)
    -   [Classification accuracy](#classification-accuracy)
-   [![alt text](tex/R_logo.svg.png "Multi-scale") Example: Spatial
    prediction of soil organic carbon using 2-scale
    model](#alt-text-example-spatial-prediction-of-soil-organic-carbon-using-2-scale-model)
    -   [Coarse-scale model](#coarse-scale-model)
    -   [Fine-scale model](#fine-scale-model)
    -   [Merge multi-scale predictions](#merge-multi-scale-predictions)
-   [Summary notes](#summary-notes)
-   [References](#references)

[<img src="tex/opengeohub_logo_ml.png" alt="OpenGeoHub logo" width="350"/>](https://opengeohub.org)

[<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />](http://creativecommons.org/licenses/by-sa/4.0/)

This work is licensed under a [Creative Commons Attribution-ShareAlike
4.0 International
License](http://creativecommons.org/licenses/by-sa/4.0/).

![alt text](tex/R_logo.svg.png "About") Introduction
----------------------------------------------------

#### Ensemble Machine Learning

Ensembles are predictive models that combine predictions from two or
more learners (Seni & Elder, [2010](#ref-seni2010ensemble); Zhang & Ma,
[2012](#ref-zhang2012ensemble)). The specific benefits of using Ensemble
learners are:

-   **Performance**: they can help improve the average prediction
    performance over any individual contributing learner in the
    ensemble.
-   **Robustness**: they can help reduce extrapolation / overshooting
    effects of individual learners.
-   **Unbiasness**: they can help determine a model-free estimate of
    prediction errors.

Even the most flexible and best performing learners such as Random
Forest or neural networks always carry a bias in the sense that the
fitting produces recognizable patterns and these are limited by the
properties of the algorithm. In the case of ensembles, the modeling
algorithm becomes secondary, and even though the improvements in
accuracy are often minor as compared to the best individual learner,
there is a good chance that the final EML model will be less prone to
overshooting and extrapolation problems.

There are in principle three ways to apply ensembles (Zhang & Ma,
[2012](#ref-zhang2012ensemble)):

-   *bagging*: learn in parallel, then combine using some deterministic
    principle (e.g. weighted averaging),
-   *boosting*: learn sequentially in an adaptive way, then combine
    using some deterministic principle,
-   *stacking*: learn in parallel, then fit a meta-model to predict
    ensemble estimates,

The “meta-model” is an additional model that basically combines all
individual or “base learners”. In this tutorial we focus only on the
stacking approach to EML.

There are several packages in R that implement Ensemble Machine
Learning, for example:

-   [SuperLearner](https://cran.r-project.org/web/packages/SuperLearner/vignettes/Guide-to-SuperLearner.html)
    package,
-   [caretEnsemble](https://cran.r-project.org/web/packages/caretEnsemble/vignettes/caretEnsemble-intro.html)
    package,
-   [h2o.stackedEnsemble](http://docs.h2o.ai/h2o-tutorials/latest-stable/tutorials/ensembles-stacking/index.html)
    package,
-   [mlr](https://mlr.mlr-org.com/reference/makeStackedLearner.html) and
    [mlr3](https://mlr3gallery.mlr-org.com/posts/2020-04-27-tuning-stacking/)
    packages,

Ensemble Machine Learning is also available in Python through the
[scikit-learn](https://scikit-learn.org/stable/modules/ensemble.html)
library.

In this tutorial we focus primarily on using the [mlr
package](https://mlr.mlr-org.com/), i.e. a wrapper functions to mlr
implemented in the landmap package.

#### Using geographical distances to improve spatial interpolation

Machine Learning was for long time been considered suboptimal for
spatial interpolation problems, in comparison to classifical
geostatistical techniques such as kriging, because it basically ignores
spatial dependence structure in the data. To incorporate spatial
dependence structures in machine learning, one can now add the so-called
“geographical features”: buffer distance, oblique distances, and/or
distances in the watershed, as features. This has shown to improve
prediction performance and produce maps that visually appear as they
have been produced by kriging (Hengl, Nussbaum, Wright, Heuvelink, &
Gräler, [2018](#ref-hengl2018random)).

Use of geographical as features in machine learning for spatial
predictions is explained in detail in:

-   Behrens, T., Schmidt, K., Viscarra Rossel, R. A., Gries, P.,
    Scholten, T., & MacMillan, R. A. (2018). [Spatial modelling with
    Euclidean distance fields and machine
    learning](https://doi.org/10.1111/ejss.12687). European journal of
    soil science, 69(5), 757-770.
-   Hengl, T., Nussbaum, M., Wright, M. N., Heuvelink, G. B., &
    Gräler, B. (2018). [Random forest as a generic framework for
    predictive modeling of spatial and spatio-temporal
    variables](https://doi.org/10.7717/peerj.5518). PeerJ, 6, e5518.
    <a href="https://doi.org/10.7717/peerj.5518" class="uri">https://doi.org/10.7717/peerj.5518</a>
-   Møller, A. B., Beucher, A. M., Pouladi, N., and Greve, M. H. (2020).
    [Oblique geographic coordinates as covariates for digital soil
    mapping](https://doi.org/10.5194/soil-6-269-2020). SOIL, 6, 269–289,
    <a href="https://doi.org/10.5194/soil-6-269-2020" class="uri">https://doi.org/10.5194/soil-6-269-2020</a>
-   Sekulić, A., Kilibarda, M., Heuvelink, G.B., Nikolić, M., Bajat, B.
    (2020). [Random Forest Spatial
    Interpolation](https://doi.org/10.3390/rs12101687). Remote Sens.
    12, 1687.
    <a href="https://doi.org/10.3390/rs12101687" class="uri">https://doi.org/10.3390/rs12101687</a>

In the case the number of covariates / features becomes large, and
assuming the covariates are diverse, and that the points are equally
spread in an area of interest, there is probably no need for using
geographical distances in model training because unique combinations of
features become so large that they can be used to represent
*geographical position* (Hengl et al., [2018](#ref-hengl2018random)).

#### EML using the landmap package

Ensemble Machine Learning (EML) based on stacking is implemented in the
mlr package (Bischl et al., [2016](#ref-bischl2016mlr)) and can be
initiated via the function e.g.:

    m = makeStackedLearner(base.learners = lrns, super.learner = "regr.ml", method = "stack.cv")

here the base learner predictions will be computed by 5-fold
cross-validation (repeated re-fitting) and then used to determine the
meta-learner. This algorithm is known as the *SuperLearner* algorithm
(Polley & van der Laan, [2010](#ref-Polley2010)).

In the case of spatial prediction, we want to *block* training points
based on spatial proximity to prevent from producing bias predictions.
For this we should know the range of spatial dependence or similar
i.e. something that can be derived by fitting a variogram, then limit
the minimum spatial distance between training and validation points to
avoid overfitting or similar.

To automate fitting of an Ensemble Machine Learning models for the
purpose of spatial interpolation / prediction, one can now use the
[landmap](https://github.com/Envirometrix/landmap) package that
combines:

-   derivation of geographical distances,
-   conversion of grids to principal components,
-   automated filling of gaps in gridded data,
-   automated fitting of variogram and determination of spatial
    auto-correlation structure,
-   spatial overlay,
-   model training using spatial Cross-Validation (Lovelace, Nowosad, &
    Muenchow, [2019](#ref-lovelace2019geocomputation)),
-   model stacking i.e. fitting of the final EML,

The concept of automating spatial interpolation until the level that
almost no human interaction is required is referred to as **automated
mapping** or automated spatial interpolation (Pebesma et al.,
[2011](#ref-pebesma2011intamap)).

To install the most recent landmap package from Github use:

    library(devtools)
    install_github("envirometrix/landmap")

![alt text](tex/R_logo.svg.png "Zinc") Example: Spatial prediction of Zinc meuse data set
-----------------------------------------------------------------------------------------

#### Interpolation of numeric values using spatial regression

We load the packages that will be used in this tutorial:

    library(landmap)
    library(rgdal)
    library(geoR)
    library(plotKML)
    library(raster)
    library(glmnet)
    library(xgboost)
    library(kernlab)
    library(deepnet)
    library(forestError)
    library(mlr)

For testing we use meuse data set. We can fit a 2D model to interpolate
zinc concentration based on sampling points, distance to the river and
flooding frequency maps by using:

    demo(meuse, echo=FALSE)
    m <- train.spLearner(meuse["zinc"], covariates=meuse.grid[,c("dist","ffreq")], 
                         lambda = 1, parallel=FALSE)

    ## Converting ffreq to indicators...

    ## Converting covariates to principal components...

    ## Deriving oblique coordinates...TRUE

    ## Fitting a variogram using 'linkfit' and trend model...TRUE

    ## Estimating block size ID for spatial Cross Validation...TRUE

    ## Using learners: regr.ranger, regr.xgboost, regr.nnet, regr.ksvm, regr.cvglmnet...TRUE

    ## Fitting a spatial learner using 'mlr::makeRegrTask'...TRUE

    ## # weights:  103
    ## initial  value 51988598.732894 
    ## final  value 20007116.935714 
    ## converged
    ## # weights:  103
    ## initial  value 46766747.861699 
    ## final  value 18054883.171429 
    ## converged
    ## # weights:  103
    ## initial  value 46017330.261004 
    ## final  value 17028246.992857 
    ## converged
    ## # weights:  103
    ## initial  value 48196632.438643 
    ## final  value 17242726.647482 
    ## converged
    ## # weights:  103
    ## initial  value 52050992.199537 
    ## final  value 19422380.489209 
    ## converged
    ## # weights:  103
    ## initial  value 44772810.236712 
    ## final  value 16323812.135714 
    ## converged
    ## # weights:  103
    ## initial  value 52157932.247753 
    ## final  value 19340089.683453 
    ## converged
    ## # weights:  103
    ## initial  value 50225562.948814 
    ## final  value 19272228.776978 
    ## converged
    ## # weights:  103
    ## initial  value 50389605.098099 
    ## final  value 19836790.742857 
    ## converged
    ## # weights:  103
    ## initial  value 52099910.258530 
    ## final  value 20016260.647482 
    ## converged
    ## # weights:  103
    ## initial  value 54840020.847197 
    ## final  value 20750447.509677 
    ## converged

    ## Fitting a quantreg model using 'ranger::ranger'...TRUE

This runs number of steps including derivation of geographical distances
(Møller, Beucher, Pouladi, & Greve, [2020](#ref-moller2020oblique)),
derivation of principal components (to make sure all features are
numeric and complete), fitting of variogram using the geoR package
(Diggle, Tawn, & Moyeed, [1998](#ref-diggle1998model)), spatial overlay,
training of individual learners and training of the super learner. The
only parameter we need to set manually in the `train.spLearner` is the
`lambda = 1` which is required to estimate variogram: in this case the
target variable is log-normally distributed, and hence the geoR package
needs the transformation parameter set at `lambda = 1`.

Note that the meta-learner is by default a linear model from five
independently fitted learners
`c("regr.ranger", "regr.xgboost", "regr.ksvm", "regr.nnet", "regr.cvglmnet")`.
We can check the success of training based on the 5-fold spatial
Cross-Validation using:

    summary(m@spModel$learner.model$super.model$learner.model)

    ## 
    ## Call:
    ## stats::lm(formula = f, data = d)
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -477.20  -94.68  -21.68   51.09 1115.73 
    ## 
    ## Coefficients:
    ##                 Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)   1602.20192  675.22033   2.373   0.0189 *  
    ## regr.ranger      0.78445    0.18445   4.253 3.71e-05 ***
    ## regr.xgboost     0.12777    0.40432   0.316   0.7524    
    ## regr.nnet       -3.41642    1.41943  -2.407   0.0173 *  
    ## regr.ksvm        0.29471    0.22673   1.300   0.1957    
    ## regr.cvglmnet   -0.07409    0.18319  -0.404   0.6865    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 208 on 149 degrees of freedom
    ## Multiple R-squared:  0.6893, Adjusted R-squared:  0.6789 
    ## F-statistic: 66.12 on 5 and 149 DF,  p-value: < 2.2e-16

Which shows that the model explains about 65% of variability in target
variable and that `regr.ranger` learner (Wright & Ziegler,
[2016](#ref-Wright2016)) is the strongest learner. Average mapping error
RMSE = 213.

To predict values at all grids we use:

    meuse.y <- predict(m)

    ## Predicting values using 'getStackedBaseLearnerPredictions'...TRUE

    ## Deriving model errors using forestError package...TRUE

    ## Warning in sqrt(pred.q$estimates$bias): NaNs produced

Note that, by default, we will predict two outputs:

-   Mean prediction: i.e. the best unbiased prediction of response,
-   Prediction errors: usually predicted as lower and upper 67%
    quantiles based on the
    [forestError](https://cran.r-project.org/package=forestError) (Lu &
    Hardin, [2021](#ref-lu2021unified))

If not otherwise specified, derivation of the prediction error (Root
Mean Square Prediction Error), bias and lower and upper prediction
intervals is implemented by default via the
[forestError](https://cran.r-project.org/package=forestError) algorithm.
The method is explained in detail in Lu & Hardin
([2021](#ref-lu2021unified)).

We could also produce the prediction intervals by using the quantreg
Random Forest algorithm (Meinshausen,
[2006](#ref-meinshausen2006quantile)) as implemented in the ranger
package, or as a standard deviation of the bootstraped models, although
using the method by Lu & Hardin ([2021](#ref-lu2021unified)) is
recommended.

To determine the prediction errors without drastically increasing
computing time, we basically fit an independent random forest model
using the five base-learners with setting `quantreg = TRUE`:

    zinc ~ regr.ranger + regr.xgboost + regr.nnet + regr.ksvm + regr.cvglmnet

The prediction error methods are non-parameteric and users can choose
any probability in the output via the `quantiles` argument. For example,
the default `quantiles` are set to produce prediction intervals for the
.682 range, which is the 1-standard-deviation range in the case of a
Gaussian distribution. Deriving prediction errors, however, can be come
computational for large number of features and trees in the random
forest, so have in mind that EML comes with exponentially increased
computing time.

We can plot the predictions and prediction errors next to each other by
using:

    par(mfrow=c(1,2), oma=c(0,0,0,1), mar=c(0,0,4,3))
    plot(raster(meuse.y$pred["response"]), col=R_pal[["rainbow_75"]][4:20],
      main="Predictions spLearner", axes=FALSE, box=FALSE)
      points(meuse, pch="+")
    plot(raster(meuse.y$pred["model.error"]), col=rev(bpy.colors()),
      main="Prediction errors", axes=FALSE, box=FALSE)
      points(meuse, pch="+")

<div class="figure">

<img src="README_files/figure-gfm/map-zinc-1.png" alt="Predicted zinc content based on meuse data set." width="70%" />
<p class="caption">
Predicted zinc content based on meuse data set.
</p>

</div>

This shows that the prediction errors (right plot) are the highest:

-   where the model is getting further away from the training points
    (spatial extrapolation),
-   where individual points with high values can not be explained by
    covariates,
-   where measured values of the response variable are in general high,

We can also plot the lower and upper prediction intervals for the .682
probability range using:

    pts = list("sp.points", meuse, pch = "+", col="black")
    spplot(meuse.y$pred[,c("q.lwr","q.upr")], col.regions=R_pal[["rainbow_75"]][4:20],
        sp.layout = list(pts),
        main="Prediction intervals (alpha = 0.318)")

<div class="figure">

<img src="README_files/figure-gfm/map-zinc-interval-1.png" alt="Lower and upper prediction intervals for zinc content based on meuse data set." width="70%" />
<p class="caption">
Lower and upper prediction intervals for zinc content based on meuse
data set.
</p>

</div>

#### Model fine-tuning and feature selection

The function `tune.spLearner` can be used to further optimize spLearner
object by:

-   fine-tuning model parameters, especially the ranger `mtry` and
    XGBoost parameters,
-   reduce number of features by running feature selection via the
    `mlr::makeFeatSelWrapper` function,

The package landmap currently requires that two base learners used
include `regr.ranger` and `regr.xgboost`, and that there are at least 3
base learners in total. The model from above can be optimized using:

    m0 <- tune.spLearner(m, xg.skip = TRUE, parallel=FALSE)

    ## Running tuneParams for ranger... TRUE

    ## [Tune] Started tuning learner regr.ranger for parameter set:

    ##          Type len Def                     Constr Req Tunable Trafo
    ## mtry discrete   -   - 2,5,8,11,14,16,19,22,25,28   -    TRUE     -

    ## With control class: TuneControlGrid

    ## Imputation value: Inf

    ## [Tune-x] 1: mtry=2

    ## [Tune-y] 1: mse.test.mean=51762.2767734; time: 0.0 min

    ## [Tune-x] 2: mtry=5

    ## [Tune-y] 2: mse.test.mean=52268.2176990; time: 0.0 min

    ## [Tune-x] 3: mtry=8

    ## [Tune-y] 3: mse.test.mean=52711.1076179; time: 0.0 min

    ## [Tune-x] 4: mtry=11

    ## [Tune-y] 4: mse.test.mean=52474.5992605; time: 0.0 min

    ## [Tune-x] 5: mtry=14

    ## [Tune-y] 5: mse.test.mean=53429.2066792; time: 0.0 min

    ## [Tune-x] 6: mtry=16

    ## [Tune-y] 6: mse.test.mean=54473.1423962; time: 0.0 min

    ## [Tune-x] 7: mtry=19

    ## [Tune-y] 7: mse.test.mean=53754.0425364; time: 0.0 min

    ## [Tune-x] 8: mtry=22

    ## [Tune-y] 8: mse.test.mean=54658.7248809; time: 0.0 min

    ## [Tune-x] 9: mtry=25

    ## [Tune-y] 9: mse.test.mean=54596.8966910; time: 0.0 min

    ## [Tune-x] 10: mtry=28

    ## [Tune-y] 10: mse.test.mean=55429.9789355; time: 0.0 min

    ## [Tune] Result: mtry=2 : mse.test.mean=51762.2767734

    ## [FeatSel] Started selecting features for learner 'regr.ranger'

    ## With control class: FeatSelControlRandom

    ## Imputation value: Inf

    ## [FeatSel-x] 1: 01111111011100111000100001010000 (16 bits)

    ## [FeatSel-y] 1: mse.test.mean=53778.4308510; time: 0.0 min

    ## [FeatSel-x] 1: 00101000010101001111000101100111 (15 bits)

    ## [FeatSel-y] 1: mse.test.mean=62738.3812869; time: 0.0 min

    ## [FeatSel-x] 1: 01010001101000001101100011100010 (13 bits)

    ## [FeatSel-y] 1: mse.test.mean=51469.4657141; time: 0.0 min

    ## [FeatSel-x] 1: 00000101110111000110010001010000 (12 bits)

    ## [FeatSel-y] 1: mse.test.mean=61114.0241724; time: 0.0 min

    ## [FeatSel-x] 1: 10010100111010100110010101001011 (16 bits)

    ## [FeatSel-y] 1: mse.test.mean=53463.7582800; time: 0.0 min

    ## [FeatSel-x] 1: 00110101111001000001111000000111 (15 bits)

    ## [FeatSel-y] 1: mse.test.mean=54984.9652818; time: 0.0 min

    ## [FeatSel-x] 1: 10100110100110110000100011100100 (14 bits)

    ## [FeatSel-y] 1: mse.test.mean=49192.7759272; time: 0.0 min

    ## [FeatSel-x] 1: 10101010100010001010011001100101 (14 bits)

    ## [FeatSel-y] 1: mse.test.mean=52738.6134078; time: 0.0 min

    ## [FeatSel-x] 1: 10001000000111001010111000011000 (12 bits)

    ## [FeatSel-y] 1: mse.test.mean=51439.8979967; time: 0.0 min

    ## [FeatSel-x] 1: 00000000110010001111001001110110 (13 bits)

    ## [FeatSel-y] 1: mse.test.mean=64284.5815740; time: 0.0 min

    ## [FeatSel-x] 1: 10000111100100000100000110001010 (11 bits)

    ## [FeatSel-y] 1: mse.test.mean=50728.5268203; time: 0.0 min

    ## [FeatSel-x] 1: 00010011010011011101001011010110 (16 bits)

    ## [FeatSel-y] 1: mse.test.mean=56877.9038662; time: 0.0 min

    ## [FeatSel-x] 1: 00010100001010101010010100100110 (12 bits)

    ## [FeatSel-y] 1: mse.test.mean=52278.8236267; time: 0.0 min

    ## [FeatSel-x] 1: 00001111011110100011100001011100 (16 bits)

    ## [FeatSel-y] 1: mse.test.mean=62236.8016232; time: 0.0 min

    ## [FeatSel-x] 1: 01000111100010000011001000110101 (13 bits)

    ## [FeatSel-y] 1: mse.test.mean=54673.5116874; time: 0.0 min

    ## [FeatSel-x] 1: 00111110010001000101110110111110 (18 bits)

    ## [FeatSel-y] 1: mse.test.mean=51884.6231030; time: 0.0 min

    ## [FeatSel-x] 1: 00111000001101010110001100011100 (14 bits)

    ## [FeatSel-y] 1: mse.test.mean=49687.3865131; time: 0.0 min

    ## [FeatSel-x] 1: 00100101011011000100111000001001 (13 bits)

    ## [FeatSel-y] 1: mse.test.mean=57512.3375508; time: 0.0 min

    ## [FeatSel-x] 1: 10100111101001100100101111110010 (18 bits)

    ## [FeatSel-y] 1: mse.test.mean=49617.3887969; time: 0.0 min

    ## [FeatSel-x] 1: 11110101110110100100011100010110 (18 bits)

    ## [FeatSel-y] 1: mse.test.mean=51966.8605676; time: 0.0 min

    ## [FeatSel] Result: PC1,PC3,rY_0,rX_0.2,rX_0.5,... (14 bits)

    ## Fiting Stacked Ensemble Model... TRUE

    ## # weights:  49
    ## initial  value 51529382.801821 
    ## final  value 19824036.000000 
    ## converged
    ## # weights:  49
    ## initial  value 45597722.203529 
    ## final  value 18573331.875000 
    ## converged
    ## # weights:  49
    ## initial  value 53814926.139446 
    ## final  value 20000324.591549 
    ## converged
    ## # weights:  49
    ## initial  value 50213506.676587 
    ## final  value 18141355.775362 
    ## converged
    ## # weights:  49
    ## initial  value 52495978.960357 
    ## final  value 19690280.429577 
    ## converged
    ## # weights:  49
    ## initial  value 51532709.293300 
    ## final  value 19498856.117647 
    ## converged
    ## # weights:  49
    ## initial  value 39709556.785937 
    ## final  value 13577708.421429 
    ## converged
    ## # weights:  49
    ## initial  value 51784999.703712 
    ## final  value 19008178.373239 
    ## converged
    ## # weights:  49
    ## initial  value 45535965.983584 
    ## final  value 18072293.703704 
    ## converged
    ## # weights:  49
    ## initial  value 51788960.512341 
    ## final  value 19915913.492958 
    ## converged
    ## # weights:  49
    ## initial  value 55025496.079028 
    ## final  value 20750447.509677 
    ## converged

    ## Fitting a quantreg model using 'ranger::ranger'...TRUE

which reports RMSE for different `mtry` and reports which features have
been left and which removed. Note that we turn off the fine-tuning of
XGboost using `xg.skip = TRUE` as it takes at the order of magnitude
more time. In summary, in this specific case, the fine-tuned model is
not much more accurate, but it comes with the less features:

    str(m0@spModel$features)

    ##  chr [1:14] "PC1" "PC3" "rY_0" "rX_0.2" "rX_0.5" "rY_0.7" "rX_1" "rX_1.2" "rY_1.2" "rX_1.9" ...

    summary(m0@spModel$learner.model$super.model$learner.model)

    ## 
    ## Call:
    ## stats::lm(formula = f, data = d)
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -518.01 -121.71  -53.90   46.15 1437.26 
    ## 
    ## Coefficients:
    ##                Estimate Std. Error t value Pr(>|t|)  
    ## (Intercept)   1113.6380   604.5790   1.842   0.0675 .
    ## regr.ranger      0.4740     0.2607   1.818   0.0711 .
    ## regr.xgboost     0.3772     0.4112   0.917   0.3604  
    ## regr.nnet       -2.4860     1.2357  -2.012   0.0460 *
    ## regr.ksvm        0.4513     0.2786   1.620   0.1074  
    ## regr.cvglmnet    0.1215     0.2322   0.523   0.6016  
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 250.8 on 149 degrees of freedom
    ## Multiple R-squared:  0.5482, Adjusted R-squared:  0.5331 
    ## F-statistic: 36.16 on 5 and 149 DF,  p-value: < 2.2e-16

Fine-tuning and feature selection can be quite computational and it is
highly recommended to start with smaller subsets of data and then
measure processing time. Note that the function
`mlr::makeFeatSelWrapper` can result in errors if the covariates have a
low variance or follow a zero-inflated distribution. Reducing the number
of features via feature selection and fine-tuning of the Random Forest
`mtry` and XGboost parameters, however, can result in significantly
higher prediction speed and can also help improve accuracy.

#### Estimation of [prediction intervals](http://www.sthda.com/english/articles/40-regression-analysis/166-predict-in-r-model-predictions-and-confidence-intervals/)

We can also print the lower and upper [prediction
interval](http://www.sthda.com/english/articles/40-regression-analysis/166-predict-in-r-model-predictions-and-confidence-intervals/)
for every location using e.g.:

    sp::over(meuse[1,], meuse.y$pred)

    ##   response model.error model.bias    q.lwr    q.upr
    ## 1 982.2807    262.1063     7.1074 639.8632 1131.559

where `q.lwr` is the lower and `q.upr` is the 68% probability upper
quantile value. This shows that the 68% probability interval for the
location `x=181072, y=333611` is about 734–1241 which means that the
prediction error (±1 s.d.), at that location, is about 250. Compare with
the actual value sampled at that location:

    meuse@data[1,"zinc"]

    ## [1] 1022

The average prediction error for the whole area is:

    summary(meuse.y$pred$model.error)

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ##   42.00   76.69  155.37  163.16  240.53  391.71

which is somewhat lower than the RMSE derived by cross-validation, but
this is also because most of the predicted values are in fact low
(skewed distribution), and EML seems not have many problems predicting
low values.

Note also, from the example above, if we refit a model using exactly the
same settings we might get somewhat different maps and different values.
This is to be expected as the number of training points and covariates
is low, the stacking is done by using (random) 5-fold Cross-validation,
and hence results will always be slightly different. The resulting
models and maps, however, should not be significantly different as this
would indicate that the EML is *unstable*.

#### Predictions using log-transformed target variable

If the purpose of spatial prediction to make a more accurate predictions
of low values of the response, then we can train a model with the
transformed variable:

    meuse$log.zinc = log1p(meuse$zinc)
    m2 <- train.spLearner(meuse["log.zinc"], covariates=meuse.grid[,c("dist","ffreq")], parallel=FALSE)

    ## Converting ffreq to indicators...

    ## Converting covariates to principal components...

    ## Deriving oblique coordinates...TRUE

    ## Fitting a variogram using 'linkfit' and trend model...TRUE

    ## Estimating block size ID for spatial Cross Validation...TRUE

    ## Using learners: regr.ranger, regr.xgboost, regr.nnet, regr.ksvm, regr.cvglmnet...TRUE

    ## Fitting a spatial learner using 'mlr::makeRegrTask'...TRUE

    ## # weights:  103
    ## initial  value 5218.230229 
    ## final  value 69.493759 
    ## converged
    ## # weights:  103
    ## initial  value 4636.778508 
    ## final  value 71.750798 
    ## converged
    ## # weights:  103
    ## initial  value 4901.084737 
    ## final  value 73.126552 
    ## converged
    ## # weights:  103
    ## initial  value 4822.235102 
    ## final  value 70.373240 
    ## converged
    ## # weights:  103
    ## initial  value 6723.707058 
    ## final  value 70.479945 
    ## converged
    ## # weights:  103
    ## initial  value 6050.868417 
    ## final  value 72.684343 
    ## converged
    ## # weights:  103
    ## initial  value 4585.362604 
    ## final  value 71.995088 
    ## converged
    ## # weights:  103
    ## initial  value 6543.959201 
    ## final  value 74.603791 
    ## converged
    ## # weights:  103
    ## initial  value 5083.460809 
    ## final  value 71.127044 
    ## converged
    ## # weights:  103
    ## initial  value 3800.019467 
    ## final  value 72.338263 
    ## converged
    ## # weights:  103
    ## initial  value 5960.987414 
    ## final  value 79.790191 
    ## converged

    ## Fitting a quantreg model using 'ranger::ranger'...TRUE

The summary model will usually have a somewhat higher R-square, but the
best learners should stay about the same:

    summary(m2@spModel$learner.model$super.model$learner.model)

    ## 
    ## Call:
    ## stats::lm(formula = f, data = d)
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -0.9507 -0.2101 -0.0688  0.1921  1.3768 
    ## 
    ## Coefficients:
    ##               Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)   26.83485   17.27039   1.554 0.122352    
    ## regr.ranger    0.77796    0.23027   3.378 0.000931 ***
    ## regr.xgboost   0.25837    0.33897   0.762 0.447147    
    ## regr.nnet     -4.62748    2.92718  -1.581 0.116029    
    ## regr.ksvm      0.28534    0.21805   1.309 0.192685    
    ## regr.cvglmnet -0.08009    0.15700  -0.510 0.610739    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.364 on 149 degrees of freedom
    ## Multiple R-squared:  0.7525, Adjusted R-squared:  0.7442 
    ## F-statistic: 90.63 on 5 and 149 DF,  p-value: < 2.2e-16

We can next predict and then back-transform the values:

    meuse.y2 <- predict(m2)

    ## Predicting values using 'getStackedBaseLearnerPredictions'...TRUE

    ## Deriving model errors using forestError package...TRUE

    ## Warning in sqrt(pred.q$estimates$bias): NaNs produced

    ## back-transform:
    meuse.y2$pred$response.t = expm1(meuse.y2$pred$response)

<div class="figure">

<img src="README_files/figure-gfm/map-zinc2-1.png" alt="Predicted zinc content based on meuse data set after log-transformation." width="70%" />
<p class="caption">
Predicted zinc content based on meuse data set after log-transformation.
</p>

</div>

If we compare distributions of two predictions (without and with
transformation) we can see that the predictions do not differ that much:

    library(ggridges)
    library(viridis)
    library(ggplot2)
    zinc.df = data.frame(zinc=c(sp::over(meuse, meuse.y$pred["response"])[,1], 
                                sp::over(meuse, meuse.y2$pred["response.t"])[,1],
                                meuse$zinc
                                ))
    zinc.df$type = as.vector(sapply(c("predicted", "log.predicted", "observed"), function(i){rep(i, nrow(meuse))}))
    ggplot(zinc.df, aes(x = zinc, y = type, fill = ..x..)) +
          geom_density_ridges_gradient(scale = 0.95, rel_min_height = 0.01, gradient_lwd = 1.) +
          scale_x_continuous(expand = c(0.01, 0)) +
          ## scale_x_continuous(trans='log2') +
          scale_y_discrete(expand = c(0.01, 0.01)) +
          scale_fill_viridis(name = "Zinc", option = "C") +
          labs(title = "Distributions comparison") +
      theme_ridges(font_size = 13, grid = TRUE) + theme(axis.title.y = element_blank())

    ## Picking joint bandwidth of 109

<div class="figure">

<img src="README_files/figure-gfm/hist-zinc2-1.png" alt="Difference in distributions observed and predicted." width="80%" />
<p class="caption">
Difference in distributions observed and predicted.
</p>

</div>

The observed very high values are somewhat smoothed out but the median
value is about the same, hence we can conclude that the two EML models
predict the target variable without a bias. To estimate the prediction
intervals using the log-transformed variable we can use:

    x = sp::over(meuse[1,], meuse.y2$pred)
    expm1(x$q.lwr); expm1(x$q.upr)

    ## [1] 764.5935

    ## [1] 1694.023

Note that the log-transformation is not needed for a non-linear learner
such ranger and/or Xgboost, but it is often a good idea if the focus of
prediction is to get a better accuracy for lower values. For example, if
the objective of spatial interpolation is to map soil nutrient
deficiencies, then log-transformation is a good idea as it will produce
slightly better accuracy for lower values.

Another advantage of using log-transformation for log-normal variables
is that the prediction intervals would most likely be symmetric, so that
derivation of prediction error (±1 s.d.) can be derived by:

    pe = (q.upr - q.lwr)/2

![alt text](tex/R_logo.svg.png "Zinc") Example: Spatial prediction of soil types (factor-variable)
--------------------------------------------------------------------------------------------------

#### EML for classification

Ensemble Machine Learning can also be used to interpolate factor type
variables e.g. soil types. This is an example with the Ebergotzen
dataset available from the package plotKML (Hengl, Roudier, Beaudette,
Pebesma, & others, [2015](#ref-hengl2015plotkml)):

    library(plotKML)
    data(eberg_grid)
    gridded(eberg_grid) <- ~x+y
    proj4string(eberg_grid) <- CRS("+init=epsg:31467")
    data(eberg)
    coordinates(eberg) <- ~X+Y
    proj4string(eberg) <- CRS("+init=epsg:31467")
    summary(eberg$TAXGRSC)

    ##     Auenboden     Braunerde          Gley         HMoor    Kolluvisol          Moor Parabraunerde 
    ##            71           790            86             1           186             1           704 
    ##  Pararendzina       Pelosol    Pseudogley        Ranker       Regosol      Rendzina          NA's 
    ##           215           252           487            20           376            23           458

In this case the target variable is `TAXGRSC` soil types based on the
German soil classification system. This changes the modeling problem
from regression to classification. We recommend using the following
learners here:

    sl.c <- c("classif.ranger", "classif.xgboost", "classif.nnTrain")

The model training and prediction however looks the same as for the
regression:

    X <- eberg_grid[c("PRMGEO6","DEMSRT6","TWISRT6","TIRAST6")]
    mF <- train.spLearner(eberg["TAXGRSC"], covariates=X, parallel=FALSE)

    ## Converting PRMGEO6 to indicators...

    ## Converting covariates to principal components...

    ## Deriving oblique coordinates...TRUE

    ## Subsetting observations to 79% complete cases...TRUE

    ## Skipping variogram modeling...TRUE

    ## Estimating block size ID for spatial Cross Validation...TRUE

    ## Using learners: classif.ranger, classif.xgboost, classif.nnTrain...TRUE

    ## Fitting a spatial learner using 'mlr::makeClassifTask'...TRUE

To generate predictions we use:

    if(!exists("TAXGRSC")){
      TAXGRSC <- predict(mF)
    }

#### Classification accuracy

By default landmap package will predict both hard classes and
probabilities per class. We can check the average accuracy of
classification by using:

    newdata = mF@vgmModel$observations@data
    sel.e = complete.cases(newdata[,mF@spModel$features])
    newdata = newdata[sel.e, mF@spModel$features]
    pred = predict(mF@spModel, newdata=newdata)
    pred$data$truth = mF@vgmModel$observations@data[sel.e, "TAXGRSC"]
    print(calculateConfusionMatrix(pred))

    ##                predicted
    ## true            Auenboden Braunerde Gley Kolluvisol Parabraunerde Pararendzina Pelosol Pseudogley
    ##   Auenboden            34         5    1          0             4            0       0          4
    ##   Braunerde             0       617    1          0            19            7       8          9
    ##   Gley                  4         8   43          1             8            0       0          4
    ##   Kolluvisol            0        14    1         89            24            0       1          6
    ##   Parabraunerde         0        23    0          1           475            0       1         10
    ##   Pararendzina          0        23    0          0             3          143       5          2
    ##   Pelosol               0         9    0          1             1            3     157          5
    ##   Pseudogley            0        44    3          3            16            2       3        328
    ##   Ranker                0        10    0          0             6            0       1          0
    ##   Regosol               0        61    0          0             9            1       3         12
    ##   Rendzina              0         2    0          0             0            0       0          0
    ##   -err.-                4       199    6          6            90           13      22         52
    ##                predicted
    ## true            Ranker Regosol Rendzina -err.-
    ##   Auenboden          0       0        0     14
    ##   Braunerde          0       7        1     52
    ##   Gley               0       0        0     25
    ##   Kolluvisol         0       3        0     49
    ##   Parabraunerde      0       3        0     38
    ##   Pararendzina       0       0        0     33
    ##   Pelosol            0       1        0     20
    ##   Pseudogley         0      12        0     83
    ##   Ranker             0       0        0     17
    ##   Regosol            0     227        0     86
    ##   Rendzina           0       0       20      2
    ##   -err.-             0      26        1    419

which shows that about 25% of classes are miss-classified and the
classification confusion is especially high for the `Braunerde` class.
Note the result above is based only on the internal training. Normally
one should repeat the process several times using 5-fold or similar (fit
EML, predict errors using resampled values only, repeat).

Predicted probabilities, however, are more interesting because they also
show where EML possibly has problems and which are the transition zones
between multiple classes:

<div class="figure">

<img src="README_files/figure-gfm/map-tax-1.png" alt="Predicted soil types based on EML." width="100%" />
<p class="caption">
Predicted soil types based on EML.
</p>

</div>

The maps show that also in this case geographical distances play a role,
but overall, the features (DTM derivatives and parnt material) seem to
be most important.

In addition to map of probabilities per class, we have also derived
errors per probability, which in this case can be computed as the
standard deviation between probabilities produced by individual learners
(for classification problems techniques such as quantreg random forest
do not exist):

<div class="figure">

<img src="README_files/figure-gfm/map-tax-error-1.png" alt="Predicted errors per soil types based on s.d. between individual learners." width="100%" />
<p class="caption">
Predicted errors per soil types based on s.d. between individual
learners.
</p>

</div>

In probability space, instead of using RMSE or similar measures, it is
often recommended to use the measures such as the
[log-loss](https://www.rdocumentation.org/packages/MLmetrics/versions/1.1.1/topics/LogLoss)
which correctly quantifies the difference between the observed and
predicted probability. As a rule of thumb, log-loss values above 0.35
indicate poor accuracy of predictions, but the threshold number for
critically low log-loss also depends on the number of classes. In the
plot above we can note that, in general, the average error in maps is
relatively low e.g. about 0.07:

    summary(TAXGRSC$pred$error.Parabraunerde)

    ##     Min.  1st Qu.   Median     Mean  3rd Qu.     Max. 
    ## 0.005339 0.065914 0.078798 0.097867 0.098594 0.373669

but there are still many pixels where confusion between classes and
prediction errors are high.

![alt text](tex/R_logo.svg.png "Multi-scale") Example: Spatial prediction of soil organic carbon using 2-scale model
--------------------------------------------------------------------------------------------------------------------

In the next example we use EML to make spatial predictions using
data-set with 2 sets of covariates basically at different resolutions
250-m and 100-m. For this we use the Edgeroi data-set (Malone,
McBratney, Minasny, & Laslett, [2009](#ref-malone2009mapping)) used
commonly in the soil science to demonstrate 3D soil mapping of soil
organic carbon (g/kg) based on samples taken from diagnostic soil
horizons (multiple depth intervals):

    data(edgeroi)
    edgeroi.sp <- edgeroi$sites
    coordinates(edgeroi.sp) <- ~ LONGDA94 + LATGDA94
    proj4string(edgeroi.sp) <- CRS("+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs")
    edgeroi.sp <- spTransform(edgeroi.sp, CRS("+init=epsg:28355"))
    out.file = paste(getwd(), "output/edgeroi/edgeroi_training_points.gpkg", sep="/")
    #if(!file.exists("out.file")){
    #  writeOGR(edgeroi.sp, out.file, layer="edgeroi_training_points", driver = "GPKG")
    #}

We can fit two independent EML’s using the two sets of covariates and
then produce final predictions by combining them. We will refer to the
two models as coarse and fine-scale models. The fine-scale models will
often be much larger datasets and require serious computing capacity.

#### Coarse-scale model

First we use the 250-m resolution covariates:

    load("input/edgeroi.grids.rda")
    gridded(edgeroi.grids) <- ~x+y
    proj4string(edgeroi.grids) <- CRS("+init=epsg:28355")
    ov2 <- over(edgeroi.sp, edgeroi.grids)
    ov2$SOURCEID <- edgeroi.sp$SOURCEID
    ov2$x = edgeroi.sp@coords[,1]
    ov2$y = edgeroi.sp@coords[,2]

This is a 3D soil data set, so we also use the horizon `DEPTH` to
explain distribution of SOC in soil:

    source("PSM_functions.R")
    h2 <- hor2xyd(edgeroi$horizons)
    ## regression matrix:
    rm2 <- plyr::join_all(dfs = list(edgeroi$sites, h2, ov2))

    ## Joining by: SOURCEID
    ## Joining by: SOURCEID

    formulaStringP2 <- ORCDRC ~ DEMSRT5+TWISRT5+EV1MOD5+EV2MOD5+EV3MOD5+DEPTH
    rmP2 <- rm2[complete.cases(rm2[,all.vars(formulaStringP2)]),]
    str(rmP2[,all.vars(formulaStringP2)])

    ## 'data.frame':    4972 obs. of  7 variables:
    ##  $ ORCDRC : num  8.5 7.3 5 4.7 4.7 ...
    ##  $ DEMSRT5: num  198 198 198 198 198 198 185 185 185 185 ...
    ##  $ TWISRT5: num  19.5 19.5 19.5 19.5 19.5 19.5 19.2 19.2 19.2 19.2 ...
    ##  $ EV1MOD5: num  1.14 1.14 1.14 1.14 1.14 1.14 -4.7 -4.7 -4.7 -4.7 ...
    ##  $ EV2MOD5: num  1.62 1.62 1.62 1.62 1.62 1.62 3.46 3.46 3.46 3.46 ...
    ##  $ EV3MOD5: num  -5.74 -5.74 -5.74 -5.74 -5.74 -5.74 0.01 0.01 0.01 0.01 ...
    ##  $ DEPTH  : num  11.5 17.5 26 55 80 ...

We can now fit an EML directly by using the derived regression matrix:

    if(!exists("m.oc")){
      m.oc = train.spLearner.matrix(rmP2, formulaStringP2, edgeroi.grids, 
                            parallel=FALSE, cov.model="nugget", cell.size=1000)
    }

The geoR here reports problems as the data set is 3D and hence there are
spatial duplicates. We can ignore this problem and use the pre-defined
cell size of 1-km for spatial blocking, although in theory one can also
fit 3D variograms and then determine blocking parameter using training
data.

The results show that the EML model is significant:

    summary(m.oc@spModel$learner.model$super.model$learner.model)

    ## 
    ## Call:
    ## stats::lm(formula = f, data = d)
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -22.082  -1.035  -0.110   0.696  62.913 
    ## 
    ## Coefficients:
    ##               Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)   -0.29891    0.19619  -1.524   0.1277    
    ## regr.ranger    0.98144    0.02501  39.237  < 2e-16 ***
    ## regr.xgboost  -0.04209    0.07314  -0.576   0.5650    
    ## regr.nnet      0.05079    0.02841   1.788   0.0739 .  
    ## regr.ksvm      0.15289    0.02948   5.186 2.24e-07 ***
    ## regr.cvglmnet -0.09829    0.04113  -2.390   0.0169 *  
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 3.152 on 4966 degrees of freedom
    ## Multiple R-squared:  0.6752, Adjusted R-squared:  0.6749 
    ## F-statistic:  2065 on 5 and 4966 DF,  p-value: < 2.2e-16

We can now predict values at e.g. 5-cm depth by adding a dummy spatial
layer with all fixed values:

    out.tif = "output/edgeroi/pred_oc_250m.tif"
    edgeroi.grids$DEPTH <- 5
    if(!exists("edgeroi.oc")){
      edgeroi.oc = predict(m.oc, edgeroi.grids[,m.oc@spModel$features])
    }
    if(!file.exists(out.tif)){
      writeGDAL(edgeroi.oc$pred["response"], out.tif, 
                options = c("COMPRESS=DEFLATE"))
      writeGDAL(edgeroi.oc$pred["model.error"], "output/edgeroi/pred_oc_250m_pe.tif", 
                options = c("COMPRESS=DEFLATE"))
    }

which shows the following:

<div class="figure">

<img src="README_files/figure-gfm/map-oc250m-1.png" alt="Predicted SOC content using 250-m covariates." width="100%" />
<p class="caption">
Predicted SOC content using 250-m covariates.
</p>

</div>

The average prediction error in the map is somewhat higher than the
average error from the model fitting:

    summary(edgeroi.oc$pred$model.error)

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ##   2.164   5.139   7.869   8.331  11.282  23.033

This is because we are predicting the top-soil SOC, which is
exponentially higher at the soil surface and hence average model errors
for top soil should be slightly larger than the mean error for the whole
soil.

#### Fine-scale model

We can now fit the fine-scale model independently from the coarse-scale
model using the 100-m resolution covariates. In this case the 100-m
covariates are based on Landsat 8 and gamma radiometrics images (see
`?edgeroi` for more details):

    load("input/edgeroi.grids100.rda")
    gridded(edgeroi.grids100) <- ~x+y
    proj4string(edgeroi.grids100) <- CRS("+init=epsg:28355")
    ovF <- over(edgeroi.sp, edgeroi.grids100)
    ovF$SOURCEID <- edgeroi.sp$SOURCEID
    ovF$x = edgeroi.sp@coords[,1]
    ovF$y = edgeroi.sp@coords[,2]
    rmF <- plyr::join_all(dfs = list(edgeroi$sites, h2, ovF))

    ## Joining by: SOURCEID
    ## Joining by: SOURCEID

    formulaStringPF <- ORCDRC ~ MVBSRT6+TI1LAN6+TI2LAN6+PCKGAD6+RUTGAD6+PCTGAD6+DEPTH
    rmPF <- rmF[complete.cases(rmF[,all.vars(formulaStringPF)]),]
    str(rmPF[,all.vars(formulaStringPF)])

    ## 'data.frame':    5001 obs. of  8 variables:
    ##  $ ORCDRC : num  8.5 7.3 5 4.7 4.7 ...
    ##  $ MVBSRT6: num  5.97 5.97 5.97 5.97 5.97 5.97 6.7 6.7 6.7 6.7 ...
    ##  $ TI1LAN6: num  31.8 31.8 31.8 31.8 31.8 31.8 14.3 14.3 14.3 14.3 ...
    ##  $ TI2LAN6: num  32.9 32.9 32.9 32.9 32.9 32.9 22.1 22.1 22.1 22.1 ...
    ##  $ PCKGAD6: num  1.39 1.39 1.39 1.39 1.39 1.39 1.06 1.06 1.06 1.06 ...
    ##  $ RUTGAD6: num  0.14 0.14 0.14 0.14 0.14 0.14 0.16 0.16 0.16 0.16 ...
    ##  $ PCTGAD6: num  7.82 7.82 7.82 7.82 7.82 7.82 6.48 6.48 6.48 6.48 ...
    ##  $ DEPTH  : num  11.5 17.5 26 55 80 ...

We fit the 2nd fine-scale model:

    if(!exists("m.ocF")){
      m.ocF = train.spLearner.matrix(rmPF, formulaStringPF, edgeroi.grids100, 
                            parallel=FALSE, cov.model="nugget", cell.size=1000)
    }
    summary(m.ocF@spModel$learner.model$super.model$learner.model)

    ## 
    ## Call:
    ## stats::lm(formula = f, data = d)
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -20.414  -0.953  -0.033   0.710  61.120 
    ## 
    ## Coefficients:
    ##                Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)   -0.464404   0.119807  -3.876 0.000107 ***
    ## regr.ranger    1.158488   0.025040  46.265  < 2e-16 ***
    ## regr.xgboost   0.005880   0.073403   0.080 0.936151    
    ## regr.nnet      0.009628   0.022740   0.423 0.672020    
    ## regr.ksvm     -0.068891   0.029658  -2.323 0.020228 *  
    ## regr.cvglmnet -0.025746   0.029850  -0.863 0.388446    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 2.959 on 4995 degrees of freedom
    ## Multiple R-squared:  0.7163, Adjusted R-squared:  0.716 
    ## F-statistic:  2522 on 5 and 4995 DF,  p-value: < 2.2e-16

which shows that the 100-m resolution covariates help make even more
accurate predictions with R-square about 0.7. We can also make
predictions at 5-cm depth by using (note: this takes almost 6x more time
to compute predictions than for 250-m resolution data):

    edgeroi.grids100$DEPTH <- 5
    if(!exists("edgeroi.ocF")){
      edgeroi.ocF = predict(m.ocF, edgeroi.grids100[,m.ocF@spModel$features])
    }
    out.tif = "output/edgeroi/pred_oc_100m.tif"
    if(!file.exists(out.tif)){
      writeGDAL(edgeroi.ocF$pred["response"], out.tif, options = c("COMPRESS=DEFLATE"))
      writeGDAL(edgeroi.ocF$pred["model.error"], "output/edgeroi/pred_oc_100m_pe.tif", options = c("COMPRESS=DEFLATE"))
    }

which shows the following:

<div class="figure">

<img src="README_files/figure-gfm/map-oc100m-1.png" alt="Predicted SOC content using 100-m covariates." width="100%" />
<p class="caption">
Predicted SOC content using 100-m covariates.
</p>

</div>

#### Merge multi-scale predictions

If we compare the coarse scale and fine scale predictions we see:

<img src="img/ebergotzen_two_scale_model_ensemble.gif" width="650" />

*Figure: Coarse-scale and fine-scale predictions of soil organic carbon
at 5-cm depth for the Edgeroi study area.*

Overall there is a match between general patterns but there are also
differences locally. This is to expect as the two models are fitted
independently using completely different covariates. We can merge the
two predictions and produce the final ensemble prediction by using the
following principles:

-   User prediction errors per pixel as weights so that more accurate
    predictions get higher weights,
-   Derive propagated error using the pooled variance based on
    individual predictions and errors,

Before we run this operation, we need to downscale the maps to the same
grid, best using Cubic-splines in GDAL:

    edgeroi.grids100@bbox

    ##       min     max
    ## x  741400  789000
    ## y 6646000 6678100

    outD.file = "output/edgeroi/pred_oc_250m_100m.tif"
    if(!file.exists(outD.file)){
      system(paste0('gdalwarp output/edgeroi/pred_oc_250m.tif ', outD.file,  
             ' -r \"cubicspline\" -te 741400 6646000 789000 6678100 -tr 100 100 -overwrite'))
      system(paste0('gdalwarp output/edgeroi/pred_oc_250m_pe.tif output/edgeroi/pred_oc_250m_100m_pe.tif',
             ' -r \"cubicspline\" -te 741400 6646000 789000 6678100 -tr 100 100 -overwrite'))
    }

We can now read the downscaled predictions, and merge them using the
prediction errors as weights (weighted average per pixel):

    edgeroi.ocF$pred$responseC = readGDAL("output/edgeroi/pred_oc_250m_100m.tif")$band1

    ## output/edgeroi/pred_oc_250m_100m.tif has GDAL driver GTiff 
    ## and has 321 rows and 476 columns

    edgeroi.ocF$pred$model.errorC = readGDAL("output/edgeroi/pred_oc_250m_100m_pe.tif")$band1

    ## output/edgeroi/pred_oc_250m_100m_pe.tif has GDAL driver GTiff 
    ## and has 321 rows and 476 columns

    X = comp.var(edgeroi.ocF$pred@data, r1="response", r2="responseC", v1="model.error", v2="model.errorC")
    edgeroi.ocF$pred$responseF = X$response
    out.tif = "output/edgeroi/pred_oc_100m_merged.tif"
    if(!file.exists(out.tif)){
      writeGDAL(edgeroi.ocF$pred["responseF"], out.tif, options = c("COMPRESS=DEFLATE"))
    }

The final map of predictions is a combination of the two independently
produced predictions:

    plot(raster(edgeroi.ocF$pred["responseF"]), col=R_pal[["rainbow_75"]][4:20],
      main="Merged predictions spLearner", axes=FALSE, box=FALSE)
    points(edgeroi.sp, pch="+")

<div class="figure">

<img src="README_files/figure-gfm/map-2scale-1.png" alt="Merged predictions (coarse+fine scale) of SOC content at 100-m." width="80%" />
<p class="caption">
Merged predictions (coarse+fine scale) of SOC content at 100-m.
</p>

</div>

To merge the prediction errors, we use the pooled variance formula
(Rudmin, [2010](#ref-rudmin2010calculating)):

    comp.var

    ## function(x, r1, r2, v1, v2){
    ##   r =  rowSums( x[,c(r1, r2)] * 1/(x[,c(v1, v2)]^2) ) / rowSums( 1/(x[,c(v1, v2)]^2) )
    ##   ## https://stackoverflow.com/questions/13593196/standard-deviation-of-combined-data
    ##   v = sqrt( rowMeans(x[,c(r1, r2)]^2 + x[,c(v1, v2)]^2) - rowMeans( x[,c(r1, r2)])^2 )
    ##   return(data.frame(response=r, stdev=v))
    ## }
    ## <bytecode: 0x55cfb8ce89d8>

    edgeroi.ocF$pred$model.errorF = X$stdev
    out.tif = "output/edgeroi/pred_oc_100m_merged_pe.tif"
    if(!file.exists(out.tif)){
      writeGDAL(edgeroi.ocF$pred["model.errorF"], out.tif, options = c("COMPRESS=DEFLATE"))
    }

So in summary, merging multi-scale predictions is a straight forward
process, but it assumes that the reliable prediction errors are
available at both sides. The pooled variance might show higher errors
where predictions between independent models differ significantly and
this is correct. The 2-scale Ensemble Machine Learning method of
Predictive Soil Mapping was used for example to produce predictions of
[soil properties and nutrients of
Africa](https://www.isda-africa.com/isdasoil/).

Summary notes
-------------

The tutorial above demonstrates how to use Ensemble Machine Learning for
predictive mapping going from numeric 2D, to factor and to 3D variables.
Have in mind that the examples shown are based on relatively small
datasets, but can still become computational if you add even more
learners. In principle we do not recommend:

-   adding learners that are significantly less accurate than your best
    learners (i.e. focus on the top 4–5 best performing learners),
-   fitting EML for &lt;50–100 training points,
-   fitting EML for spatial interpolation where points are heavily
    spatially clustered,
-   using landmap package with large datasets,

For derivation of prediction error and prediction interval we recommend
using the method of Lu & Hardin ([2021](#ref-lu2021unified)). This
method by default produces three measures of uncertainty:

-   Root Mean Square Prediction Error (RMSPE) = the estimated
    conditional mean squared prediction errors of the random forest
    predictions,
-   bias = the estimated conditional biases of the random forest
    predictions,
-   lower and upper bounds / prediction intervals for a given
    probability e.g. 0.05 for the 95% probability interval,

You can also follow an introduction to Ensemble Machine Learning from
the [OpenGeoHub Summer School 2020 video
recordings](https://www.youtube.com/playlist?list=PLXUoTpMa_9s0Ea--KTV1OEvgxg-AMEOGv).

Please note that the mlr package is discontinued, so some of the example
above might become unstable with time. We are working on migrating the
code in the landmap package to make the `train.spLearner` function work
with the new [mlr3 package](https://mlr3.mlr-org.com/).

If you have a dataset that you have used to test Ensemble Machine
Learning, please come back to us and share your experiences by posting
[an issue](https://github.com/Envirometrix/landmap/issues) and/or
providing a screenshot of your results.

References
----------

<div id="refs" class="references hanging-indent">

<div id="ref-bischl2016mlr">

Bischl, B., Lang, M., Kotthoff, L., Schiffner, J., Richter, J.,
Studerus, E., … Jones, Z. M. (2016). mlr: Machine Learning in R. *The
Journal of Machine Learning Research*, *17*(1), 5938–5942.

</div>

<div id="ref-diggle1998model">

Diggle, P. J., Tawn, J. A., & Moyeed, R. A. (1998). Model-based
geostatistics. *Journal of the Royal Statistical Society: Series C
(Applied Statistics)*, *47*(3), 299–350.

</div>

<div id="ref-hengl2018random">

Hengl, T., Nussbaum, M., Wright, M. N., Heuvelink, G. B., & Gräler, B.
(2018). Random forest as a generic framework for predictive modeling of
spatial and spatio-temporal variables. *PeerJ*, *6*, e5518.

</div>

<div id="ref-hengl2015plotkml">

Hengl, T., Roudier, P., Beaudette, D., Pebesma, E., & others. (2015).
PlotKML: Scientific visualization of spatio-temporal data. *Journal of
Statistical Software*, *63*(5), 1–25.

</div>

<div id="ref-lovelace2019geocomputation">

Lovelace, R., Nowosad, J., & Muenchow, J. (2019). *Geocomputation with
r*. CRC Press.

</div>

<div id="ref-lu2021unified">

Lu, B., & Hardin, J. (2021). A unified framework for random forest
prediction error estimation. *Journal of Machine Learning Research*,
*22*(8), 1–41.

</div>

<div id="ref-malone2009mapping">

Malone, B. P., McBratney, A., Minasny, B., & Laslett, G. (2009). Mapping
continuous depth functions of soil carbon storage and available water
capacity. *Geoderma*, *154*(1-2), 138–152.

</div>

<div id="ref-meinshausen2006quantile">

Meinshausen, N. (2006). Quantile regression forests. *Journal of Machine
Learning Research*, *7*(Jun), 983–999.

</div>

<div id="ref-moller2020oblique">

Møller, A. B., Beucher, A. M., Pouladi, N., & Greve, M. H. (2020).
Oblique geographic coordinates as covariates for digital soil mapping.
*SOIL*, *6*(2), 269–289.

</div>

<div id="ref-pebesma2011intamap">

Pebesma, E., Cornford, D., Dubois, G., Heuvelink, G. B., Hristopulos,
D., Pilz, J., … Skøien, J. O. (2011). INTAMAP: The design and
implementation of an interoperable automated interpolation web service.
*Computers & Geosciences*, *37*(3), 343–352.

</div>

<div id="ref-Polley2010">

Polley, E. C., & van der Laan, M. J. (2010). *Super learner in
prediction*. U.C. Berkeley Division of Biostatistics. Retrieved from
<https://biostats.bepress.com/ucbbiostat/paper266>

</div>

<div id="ref-rudmin2010calculating">

Rudmin, J. W. (2010). Calculating the exact pooled variance. *arXiv
Preprint*, *1007.1012*.

</div>

<div id="ref-seni2010ensemble">

Seni, G., & Elder, J. F. (2010). *Ensemble methods in data mining:
Improving accuracy through combining predictions*. Morgan & Claypool
Publishers.

</div>

<div id="ref-Wright2016">

Wright, M. N., & Ziegler, A. (2016). ranger: A Fast Implementation of
Random Forests for High Dimensional Data in C++ and R. *Journal of
Statistical Software*, (in press), 18.

</div>

<div id="ref-zhang2012ensemble">

Zhang, C., & Ma, Y. (2012). *Ensemble machine learning: Methods and
applications*. Springer New York.

</div>

</div>
